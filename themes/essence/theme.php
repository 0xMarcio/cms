<?php global $Cms ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="title" content="<?= $Cms->get('config', 'siteTitle') ?> - <?= $Cms->page('title') ?>" />
		<meta name="description" content="<?= $Cms->page('description') ?>">
		<meta name="keywords" content="<?= $Cms->page('keywords') ?>">

		<meta property="og:url" content="<?= $this->url() ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="<?= $Cms->get('config', 'siteTitle') ?>" />
		<meta property="og:title" content="<?= $Cms->page('title') ?>" />
		<meta name="twitter:site" content="<?= $this->url() ?>" />
		<meta name="twitter:title" content="<?= $Cms->get('config', 'siteTitle') ?> - <?= $Cms->page('title') ?>" />
		<meta name="twitter:description" content="<?= $Cms->page('description') ?>" />

		<title><?= $Cms->get('config', 'siteTitle') ?> - <?= $Cms->page('title') ?></title>

		<link rel="stylesheet" rel="preload" as="style" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link rel="stylesheet" rel="preload" as="style" href="<?= $Cms->asset('css/style.css') ?>">

		<?= $Cms->css() ?>

	</head>

	<body class="d-flex flex-column">
		<?= $Cms->settings() ?>
		<?= $Cms->alerts() ?>

		<nav class="navbar navbar-expand-lg navbar-light navbar-default">
			<div class="container">
				<a class="navbar-brand" href="<?= $Cms->url() ?>">
					<?= $Cms->siteTitle() ?>
				</a>

				<div class="navbar-header">
					<button type="button" class="navbar-toggler navbar-toggle" data-toggle="collapse" data-target="#menu-collapse">
						<span class="navbar-toggler-icon">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="menu-collapse">
					<ul class="nav navbar-nav navbar-right ml-auto">
						<?= $Cms->menu() ?>
					</ul>
				</div>
			</div>
		</nav>

		<section class="container mt-5 mb-5">
			<div class="row">
				<div class="col-lg-12 my-auto text-center padding40">
					<?= $Cms->page('content') ?>

				</div>
			</div>
		</section>

		<div class="h-05"></div>

		<section class="container-fluid mt-5 mb-5 flex-grow">
			<div class="row customBackground">
				<div class="col-lg-12 my-auto text-center padding40 resetTextRotation">
					<?= $Cms->block('subside') ?>

				</div>
			</div>
		</section>

		<footer class="mt-4 footer">
			<div class="container-fluid py-3 text-right">
				<?= $Cms->footer() ?>
			</div>
		</footer>

		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous" defer></script>
		<?= $Cms->js() ?>

	</body>
</html>
