# simple-statistics
[![Issue Progress](https://img.shields.io/badge/%E2%9C%93-Issue%20Progress-gray?labelColor=brightgreen&style=flat)](https://crypt.stanisic.nl/kanban/#/2/kanban/view/p6mqokEiUAhkSAJsJVWJyDn04dYvNAkWBLtt4PRF7ZU/)
[![CMS 3.x.x](https://img.shields.io/badge/CMS-3.x.x-%231ab?style=flat)](https://github.com/robiso/wondercms)

A privacy minded analytics plugin

![](preview.jpg)
