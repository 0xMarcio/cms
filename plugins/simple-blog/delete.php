<?php

define("PHPUNIT_TESTING", true);

include_once("../../index.php");

$Cms = new Cms();
$Cms->init();

$SimpleBlog = new SimpleBlog(false);
$SimpleBlog->init();

$requestToken = $_POST['token'] ?? $_GET['token'] ?? null;
if(!$Cms->loggedIn
    || $_SESSION['token'] !== $requestToken
    || !$Cms->hashVerify($requestToken))
    die("Please login first.");

if(!isset($_GET["page"])) die("Please specify key and value");

$slug = $Cms->slugify($_GET["page"]);

if(empty($slug)) die("Please specify all the fields");

$posts = (array)$SimpleBlog->get("posts");

if(isset($posts[$slug]))
    unset($posts[$slug]);

$SimpleBlog->set("posts", $posts);

header("location: " . $Cms->url("../../{$SimpleBlog->slug}"));

?>
