<?php

define("PHPUNIT_TESTING", true);

include_once("../../index.php");

$Cms = new Cms();
$Cms->init();

$SimpleBlog = new SimpleBlog(false);
$SimpleBlog->init();

$requestToken = $_POST['token'] ?? $_GET['token'] ?? null;
if(!$Cms->loggedIn
    || $_SESSION['token'] !== $requestToken
    || !$Cms->hashVerify($requestToken))
    die("Please login first.");

if(!isset($_POST["key"], $_POST["value"], $_POST["page"])) die("Please specify key and value");

$key = $Cms->slugify($_POST["key"]);
$page = $Cms->slugify($_POST["page"]);
$value = $_POST["value"];

if(empty($key) || empty($page) || empty($value)) die("Please specify all the fields");

$SimpleBlog->set("posts", $page, $key, $value);

?>
