<?php
/**
 * Summernote editor plugin.
 *
 * It transforms all the editable areas into the Summernote inline editor.
 *
 * @author Prakai Nadee <prakai@rmuti.acth>
 * @forked by Robert Isoski @robertisoski
 */

global $Cms;

if (defined('VERSION')) {
    $Cms->addListener('js', 'loadSummerNoteJS');
    $Cms->addListener('css', 'loadSummerNoteCSS');
    $Cms->addListener('editable', 'initialSummerNoteVariables');
}

function initialSummerNoteVariables($contents) {
    global $Cms;
    $content = $contents[0];
    $subside = $contents[1];

    $contents_path = $Cms->getConfig('contents_path');
    if (!$contents_path) {
        $Cms->setConfig('contents_path', $Cms->filesPath);
        $contents_path = $Cms->filesPath;
    }
    $contents_path_n = trim($contents_path, "/");
    if ($contents_path != $contents_path_n) {
        $contents_path = $contents_path_n;
        $Cms->setConfig('contents_path', $contents_path);
    }
    $_SESSION['contents_path'] = $contents_path;

    return array($content, $subside);
}

function loadSummerNoteJS($args) {
    global $Cms;
    if ($Cms->loggedIn) {
        $script = <<<EOT
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
        <script src="{$Cms->url('plugins/summernote-editor/js/admin.js')}" type="text/javascript"></script>
        <script src="{$Cms->url('plugins/summernote-editor/js/files.js')}" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

EOT;
        $args[0] .= $script;
    }
    return $args;
}

function loadSummerNoteCSS($args) {
    global $Cms;
    if ($Cms->loggedIn) {
        $script = <<<EOT
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{$Cms->url('plugins/summernote-editor/css/admin.css')}" type="text/css" media="screen">
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
EOT;
        $args[0] .= $script;
    }
    return $args;
}
