{
    "config": {
        "siteTitle": "CMS",
        "theme": "fresh",
        "defaultPage": "home",
        "login": "_admin",
        "forceLogout": false,
        "password": "$2y$10$BW0KkFIECkpxf39.9Vq1Su5wu.6UWbuqGVwUHvS9fob8IIJXEXz.m",
        "lastLogins": {
            "2021\/09\/27 23:09:58": "89.177.155.93",
            "2021\/09\/27 22:14:33": "89.177.155.93",
            "2021\/09\/27 22:04:45": "89.177.155.93"
        },
        "defaultRepos": {
            "themes": {
                "0": ""
            },
            "plugins": {
                "0": ""
            },
            "lastSync": "2021\/09\/27"
        },
        "customRepos": {
            "themes": {
                "0": "https:\/\/github.com\/0xMarcio\/fresh"
            },
            "plugins": {}
        },
        "menuItems": {
            "0": {
                "name": "Home",
                "slug": "home",
                "visibility": "show"
            },
            "1": {
                "name": "Changelog",
                "slug": "changelog",
                "visibility": "show"
            },
            "2": {
                "name": "Example",
                "slug": "example",
                "visibility": "show"
            }
        }
    },
    "pages": {
        "404": {
            "title": "404",
            "keywords": "404",
            "description": "404",
            "content": "<h1>Sorry, page not found. :(<\/h1>"
        },
        "home": {
            "title": "Home",
            "keywords": "Keywords, are, good, for, search, enginesasdasd",
            "description": "A short description is also good.",
            "content": "<div class=\"hero-body\">\n        <div class=\"container\">\n            <div class=\"columns is-vcentered\">\n                <div class=\"column is-5 is-offset-1 landing-caption\">\n                    <h1 class=\"title is-1 is-bold is-spaced\">\n                        Manage, Deploy.\n                    <\/h1>\n                    <h2 class=\"subtitle is-5 is-muted\">Lorem ipsum sit dolor amet is a dummy text used by typography industry <\/h2>\n                    <div class=\"button-wrap\">\n                        <a class=\"button cta is-rounded primary-btn raised\">\n                            Get Started\n                        <\/a>\n                        <a class=\"button cta is-rounded\">\n                            Discover\n                        <\/a>\n                    <\/div>\n                <\/div>\n                <div class=\"column is-5\">\n                    <figure class=\"image is-4by3\">\n                        <img src=\"\/themes\/fresh\/img\/illustrations\/worker.svg\" alt=\"Description\">\n                    <\/figure>\n                <\/div>\n\n            <\/div>\n        <\/div>\n\n    <\/div>"
        },
        "example": {
            "title": "Example",
            "keywords": "Keywords, are, good, for, search, engines",
            "description": "A short description is also good.",
            "content": "<h1 class=\"mb-3\">Easy editing<\/h1>\n<p>Click anywhere to edit, click outside the area to save. Changes are live and shown immediately.<\/p>\n\n<h2 class=\"mt-5 mb-3\">Create new page<\/h2>\n<p>Pages can be created in the Menu above.<\/p>"
        },
        "changelog": {
            "title": "Changelog"
        }
    },
    "blocks": {
        "subside": {
            "content": "<h2>About your website<\/h2>\n\n<br>\n<p>Website description, contact form, mini map or anything else.<\/p>\n<p>This editable area is visible on all pages.<\/p>"
        },
        "footer": {
            "content": "--- FOOTER ---"
        },
        "fresh-footer": {
            "content": "<footer class=\"footer footer-dark\">\n        <div class=\"container\">\n            <div class=\"columns\">\n                <div class=\"column\">\n                    <div class=\"footer-logo\">\n                        <img src=\"\/themes\/fresh\/img\/logo\/fresh-white-alt.svg\">\n                    <\/div>\n                <\/div>\n                <div class=\"column\">\n                    <div class=\"footer-column\">\n                        <div class=\"footer-header\">\n                            <h3>Product<\/h3>\n                        <\/div>\n                        <ul class=\"link-list\">\n                            <li><a href=\"#\">Discover features<\/a><\/li>\n                            <li><a href=\"#\">Why choose our Product ?<\/a><\/li>\n                            <li><a href=\"#\">Compare features<\/a><\/li>\n                            <li><a href=\"#\">Our Roadmap<\/a><\/li>\n                            <li><a href=\"#\">Request features<\/a><\/li>\n                        <\/ul>\n                    <\/div>\n                <\/div>\n                <div class=\"column\">\n                    <div class=\"footer-column\">\n                        <div class=\"footer-header\">\n                            <h3>Docs<\/h3>\n                        <\/div>\n                        <ul class=\"link-list\">\n                            <li><a href=\"#\">Get Started<\/a><\/li>\n                            <li><a href=\"#\">User guides<\/a><\/li>\n                            <li><a href=\"#\">Admin guide<\/a><\/li>\n                            <li><a href=\"#\">Developers<\/a><\/li>\n                        <\/ul>\n                    <\/div>\n                <\/div>\n                <div class=\"column\">\n                    <div class=\"footer-column\">\n                        <div class=\"footer-header\">\n                            <h3>Blogroll<\/h3>\n                        <\/div>\n                        <ul class=\"link-list\">\n                            <li><a href=\"#\">Latest News<\/a><\/li>\n                            <li><a href=\"#\">Tech articles<\/a><\/li>\n                            <li><a href=\"#\">Video Blog<\/a><\/li>\n                        <\/ul>\n                    <\/div>\n                <\/div>\n                <div class=\"column\">\n                    <div class=\"footer-column\">\n                        <div class=\"footer-header\">\n                            <h3>Follow Us<\/h3>\n                            <nav class=\"level is-mobile\">\n                                <div class=\"level-left\">\n                                    <a class=\"level-item\" href=\"https:\/\/github.com\/#\">\n                                        <span class=\"icon\">\n                                            <ion-icon name=\"logo-github\" size=\"large\"><\/ion-icon>\n                                        <\/span>\n                                    <\/a>\n                                    <a class=\"level-item\" href=\"https:\/\/facebook.com\/#\">\n                                        <span class=\"icon\">\n                                            <ion-icon name=\"logo-facebook\" size=\"large\"><\/ion-icon>\n                                        <\/span>\n                                    <\/a>\n                                    <a class=\"level-item\" href=\"https:\/\/google.com\/#\">\n                                        <span class=\"icon\">\n                                            <ion-icon name=\"logo-google\" size=\"large\"><\/ion-icon>\n                                        <\/span>\n                                    <\/a>\n                                    <a class=\"level-item\" href=\"https:\/\/linkedin.com\/#\">\n                                        <span class=\"icon\">\n                                            <ion-icon name=\"logo-linkedin\" size=\"large\"><\/ion-icon>\n                                        <\/span>\n                                    <\/a>\n                                <\/div>\n                            <\/nav>\n\n                            <a href=\"https:\/\/bulma.io\" target=\"_blank\">\n                                <img src=\"\/themes\/fresh\/img\/logo\/made-with-bulma.png\" alt=\"Made with Bulma\" width=\"128\" height=\"24\">\n                            <\/a>\n                        <\/div>\n                    <\/div>\n                <\/div>\n            <\/div>\n        <\/div>\n    <\/footer>"
        }
    }
}